//
//  ViewController.swift
//  ARPathWay
//
//  Created by Haris Shobaruddin Roabbni on 17/08/19.
//  Copyright © 2019 Haris Shobaruddin Robbani. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import FocusNode
import SmartHitTest
import SCNPath

//extension ARSCNView: ARSmartHitTest{}

class ViewController: UIViewController, ARSCNViewDelegate {

//    @IBOutlet var sceneView: ARSCNView!
    var sceneView = ARSCNView(frame: .zero)
    let focusNode = FocusSquare()
    let pathNode = SCNPathNode(path: [])
    var hitPoints = [SCNVector3]() {
        didSet {
            self.pathNode.path = self.hitPoints
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.frame = self.view.bounds
        self.view.addSubview(sceneView)
        self.sceneView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        sceneView.delegate = self
        sceneView.showsStatistics = true
        
        self.focusNode.viewDelegate = sceneView
        sceneView.scene.rootNode.addChildNode(self.focusNode)
        
        setupGestures()
    }
    
    func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        tapGesture.delegate = self as? UIGestureRecognizerDelegate
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @IBAction func handleTap(_ gestureRecognizer: UITapGestureRecognizer) {
        guard gestureRecognizer.state == .ended else {
            return
        }
        if self.focusNode.state != .initializing {
            print(self.focusNode.position)
            self.hitPoints.append(self.focusNode.position)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal, .vertical]

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    // MARK: - ARSCNViewDelegate
    
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        DispatchQueue.main.async {
            self.focusNode.updateFocusNode()
        }
    }
    
    func renderer(
        _ renderer: SCNSceneRenderer,
        didAdd node: SCNNode, for anchor: ARAnchor
        ) {
        if let planeAnchor = anchor as? ARPlaneAnchor,
            planeAnchor.alignment == .vertical,
            let geom = ARSCNPlaneGeometry(device: MTLCreateSystemDefaultDevice()!)
        {
            geom.update(from: planeAnchor.geometry)
            geom.firstMaterial?.colorBufferWriteMask = .alpha
            node.geometry = geom
        }
    }
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if let planeAnchor = anchor as? ARPlaneAnchor,
            planeAnchor.alignment == .vertical,
            let geom = node.geometry as? ARSCNPlaneGeometry
        {
            geom.update(from: planeAnchor.geometry)
        }
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
